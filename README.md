Elasticsearch integrated with MongoDB
=====================================

Ubuntu Dockerfile for Elasticsearch with MongoDB connector.

To check if Docker is correctly installed and running:

	# docker version


To build:

	# docker build -rm -t <USERNAME>/elasticmongo .


To run:
First start MongoDB replicaset container, with the name mongohost. Then link that container to the elasticsearch container as shown below.

	# docker run --name mongohost -i -t mongo --replSet rs0 --smallfiles
	# docker run --name elastic -i -t --link mongohost:mongohost ganeshravi/elasticmongo


To use Kibana for data visualisation:

	# docker run --name kibana -i -t -p 8080:8080 -e ES_HOST=$(docker inspect --format {{.NetworkSettings.IPAddress}} elastic) ganeshravi/kibana
