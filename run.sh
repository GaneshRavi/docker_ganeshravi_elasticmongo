#!/bin/bash

echo "rs.initiate()" > replset_starter
mongo --host mongohost < replset_starter

echo "rs.isMaster()" > is_master_check
is_master_result=`mongo --host mongohost < is_master_check`

expected_result="\"ismaster\" : true"

while true;
do
  if [ "${is_master_result/$expected_result}" = "$is_master_result" ] ; then
    echo "Waiting for Mongod node to assume primary status..."
    sleep 1
    is_master_result=`mongo --host mongohost < is_master_check`
  else
    echo "Mongod node is now primary"
    break;
  fi
done

mongo-connector -m mongohost:27017 -t localhost:9200 -d /usr/local/lib/python2.7/dist-packages/mongo_connector/doc_managers/elastic_doc_manager.py&

/elasticsearch/bin/elasticsearch
